### Hello actix-web

https://github.com/actix/actix-web

#### Build image:

```shell
  docker build -t hello --force-rm --no-cache -f  Dockerfile .
```

#### Run image:

```shell
  
  echo "== start hello"
  docker run -d -p 5000:5000 hello &
  docker ps

  echo "== wait 3s for startup"
  sleep 3s

  echo "== curl both routes"
  curl http://localhost:5000
  curl http://localhost:5000/hello 

```