#[macro_use]
extern crate actix_web;

use actix_web::{web, App, HttpResponse, HttpServer, Responder};

#[get("/")]
async fn index() -> impl Responder {
    println!("GET: /");
    HttpResponse::Ok().body("Hello world!")
}

#[get("/hello")]
async fn hello() -> impl Responder {
    println!("GET: /hello");
    HttpResponse::Ok().body("Hello world again!")
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    println!("Starting actix-web server");

    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(hello)
    })
    .bind("0.0.0.0:5000")?
    .run()
    .await
}