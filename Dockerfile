FROM rust:1 as builder

COPY . .

RUN cargo build --release

FROM rust:1-slim-stretch

COPY --from=builder /target/release/hello .

RUN ls -la /hello

EXPOSE 5000

ENTRYPOINT ["/hello"]
